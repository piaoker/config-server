package com.doorcii.configserver.services;

import org.tio.client.ClientChannelContext;
import org.tio.core.Aio;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.Type;


/**
 * 登陆服务
 * @author Jacky
 */
public class TestService {
	public static void testSend(ClientChannelContext<ConfigContext, TPacket, Object> channelContext) {
		TPacket tpacket = TPacket.buildTPacket(Type.JOIN_GROUP_REQ_PACKET);
		// 登陆失败立即清空
		Aio.bSend(channelContext, tpacket);
	}
	
}
