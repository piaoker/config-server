package com.doorcii.configserver.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.handler.ConfigAbstractHandler;
import com.doorcii.configserver.msgbeans.TConfigBean;
import com.doorcii.configserver.serializers.Serializer;

/**
 * 登陆响应
 * @author Jacky
 * 2017年5月14日
 */
public class PullServiceRespHandler extends ConfigAbstractHandler<TConfigBean> {
	private static Logger logger = LoggerFactory.getLogger(PullServiceRespHandler.class);
	
	@Override
	public Object handler(TConfigBean configBean,
			ChannelContext<ConfigContext, TPacket, Object> channelContext) throws Exception {
		logger.debug("received config server info :{}",configBean);
		return null;
	}

	
	@Override
	public TConfigBean deserial(Serializer<byte[], TConfigBean> serial, byte[] body) {
		if(null == body) return null;
		return serial.deSerialize(body, TConfigBean.class);
	}
	
}
