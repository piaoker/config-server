package com.doorcii.configserver.services;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.Aio;
import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.Type;
import com.doorcii.configserver.commons.UserInfo;
import com.doorcii.configserver.msgbeans.PullReqBean;
import com.doorcii.configserver.serializers.SerialSaveMap;
import com.doorcii.configserver.serializers.Serializer;

/**
 * 配置拉取服务
 * @author Jacky
 */
public class ConfigService {
	private static Logger logger = LoggerFactory.getLogger(ConfigService.class);
	
	public static void pullConfig(ChannelContext<ConfigContext, TPacket, Object> channelContext) {
		TPacket tpacket = TPacket.buildTPacket(Type.IP_LIST_REQ_PACKET);
		UserInfo userInfo = channelContext.getSessionContext().getUserInfo();
		if(StringUtils.isBlank(userInfo.getAccessToken())) {
			logger.error("access token is null.");
			return;
		}
		PullReqBean pullBean = new PullReqBean(userInfo.getAccessToken());
		Serializer<byte[],PullReqBean> serializer = SerialSaveMap.getKryoSerializer();
		byte[] body = serializer.serialize(pullBean);
		tpacket.setBody(body);
		Aio.send(channelContext, tpacket);
	}
}
