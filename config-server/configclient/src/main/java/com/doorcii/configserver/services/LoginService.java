package com.doorcii.configserver.services;

import org.tio.client.ClientChannelContext;
import org.tio.core.Aio;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.Constants;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.Type;
import com.doorcii.configserver.commons.UserInfo;
import com.doorcii.configserver.msgbeans.LoginReqBean;
import com.doorcii.configserver.serializers.SerialSaveMap;
import com.doorcii.configserver.serializers.Serializer;


/**
 * 登陆服务
 * @author Jacky
 */
public class LoginService {
	private static final String userName = "test";
	private static final String password = "***";
	private static final String groupName = "t_group";
	
	public static void login(ClientChannelContext<ConfigContext, TPacket, Object> channelContext) {
		TPacket tpacket = TPacket.buildTPacket(Type.LOGIN_REQ_PACKET);
		LoginReqBean req = new LoginReqBean();
		req.setUserName(userName);
		req.setPassword(password);
		req.setGroupName(groupName);
		req.setAppId(Constants.clientAppId);
		Serializer<byte[],LoginReqBean> serializer = SerialSaveMap.getKryoSerializer();
		byte[] body = serializer.serialize(req);
		tpacket.setBody(body);
		UserInfo userInfo = new UserInfo();
		userInfo.setAppId(Constants.clientAppId);
		userInfo.setGroupName(groupName);
		userInfo.setUserName(userName);
		userInfo.setPassword(password);
		channelContext.setSessionContext(new ConfigContext(userInfo));
		// 登陆失败立即清空
		Aio.send(channelContext, tpacket);
	}
	
	public static void bindGroup() {
		
	}
	
}
