package com.doorcii.configserver.client;

import java.util.HashMap;
import java.util.Map;

import org.tio.client.intf.ClientAioHandler;
import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.ConfigServerAbstractHandler;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.Type;
import com.doorcii.configserver.handler.ConfigAbstractHandler;
import com.doorcii.configserver.handlers.LoginRespHandler;
import com.doorcii.configserver.handlers.PullServiceRespHandler;

public class ConfigServerClientHandler extends ConfigServerAbstractHandler implements ClientAioHandler<ConfigContext, TPacket, Object>{
	
	private static Map<Byte, ConfigAbstractHandler<?>> handlerMap = new HashMap<>();
	static {
		handlerMap.put(Type.LOGIN_RESP_PACKET, new LoginRespHandler());
		handlerMap.put(Type.IP_LIST_RESP_PACKET, new PullServiceRespHandler());
	}
	
	@Override
	public Object handler(TPacket packet, ChannelContext<ConfigContext, TPacket, Object> channelContext)
			throws Exception {
		ConfigAbstractHandler<?> handler = handlerMap.get(packet.getType());
		if(null != handler) handler.handler(packet, channelContext);
		return null;
	}
	
	private static TPacket heartbeatPacket = TPacket.buildTPacket(Type.HEART_BEAT_PACKET);
	@Override
	public TPacket heartbeatPacket() {
		return heartbeatPacket;
	}
}
