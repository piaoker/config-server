package com.doorcii.configserver.client;

import org.tio.client.AioClient;
import org.tio.client.ClientChannelContext;
import org.tio.client.ClientGroupContext;
import org.tio.client.ReconnConf;
import org.tio.client.intf.ClientAioHandler;
import org.tio.client.intf.ClientAioListener;
import org.tio.core.DefaultAioListener;
import org.tio.core.Node;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.Constants;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.services.LoginService;
import com.doorcii.configserver.services.TestService;

/**
 * config-server客户端启动
 * @author Jacky
 * 2017年5月14日
 * 用于和spring做集成
 */
public class ConfigServerClientStarter {
	
	private Node serverNode = new Node(Constants.LINUX_SERVER, Constants.PORT);
	private ReconnConf<ConfigContext, TPacket, Object> reconnConf = new ReconnConf<ConfigContext, TPacket, Object>(5000L);

	private ClientAioHandler<ConfigContext, TPacket, Object> clientHandler = new ConfigServerClientHandler();
	private ClientAioListener<ConfigContext, TPacket, Object> aioListener = new DefaultAioListener<>();
	private ClientGroupContext<ConfigContext, TPacket, Object> clientGroupContext = new ClientGroupContext<>(clientHandler, aioListener, reconnConf);

	private AioClient<ConfigContext, TPacket, Object> aioClient = null;
	private ClientChannelContext<ConfigContext, TPacket, Object> clientChannelContext;
	
	public void init() throws Exception {
		aioClient = new AioClient<>(clientGroupContext);
		clientChannelContext = aioClient.connect(serverNode);
		//login();
	}
	
	public void login() {
		LoginService.login(clientChannelContext);
	}
	
	public void testSend() {
		long start = System.currentTimeMillis();
		for(int i=0;i<100000;i++) {
			TestService.testSend(clientChannelContext);
		}
		long end = System.currentTimeMillis();
		System.out.println("吞吐量："+(100000*1000/(end-start))+"次/s");
	}
	
	public void destory() throws Throwable {
		aioClient.stop();
		clientChannelContext = null;
		aioClient = null;
		clientHandler = null;
		reconnConf = null;
		serverNode = null;
		super.finalize();
	}


	public static void main(String[] args) throws Throwable {
		ConfigServerClientStarter starter = new ConfigServerClientStarter();
		starter.init();
		//starter.login();
		//starter.testSend();
	}
}
