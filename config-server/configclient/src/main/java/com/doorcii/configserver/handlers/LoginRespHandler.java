package com.doorcii.configserver.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.UserInfo;
import com.doorcii.configserver.handler.ConfigAbstractHandler;
import com.doorcii.configserver.msgbeans.LoginRespBean;
import com.doorcii.configserver.serializers.Serializer;
import com.doorcii.configserver.services.ConfigService;

/**
 * 登陆响应
 * @author Jacky
 * 2017年5月14日
 */
public class LoginRespHandler extends ConfigAbstractHandler<LoginRespBean> {
	private static Logger logger = LoggerFactory.getLogger(LoginRespHandler.class);
	
	@Override
	public Object handler(LoginRespBean loginRespBody,
			ChannelContext<ConfigContext, TPacket, Object> channelContext) throws Exception {
		logger.debug("received body:{}",loginRespBody);
		if(loginRespBody.isLoginSuc()) {
			String accessToken = loginRespBody.getAccessToken();
			long loginTime = loginRespBody.getLoginTime();
			UserInfo userInfo = channelContext.getSessionContext().getUserInfo();
			userInfo.setAccessToken(accessToken);
			logger.debug("user:{} login success.login time:{}",userInfo.getUserName(),loginTime);
			// 拉取服务端列表请求
			ConfigService.pullConfig(channelContext);
		} else {
			channelContext.setSessionContext(null);
			logger.error("login failed!:{}",loginRespBody);
		}
		return null;
	}

	
	@Override
	public LoginRespBean deserial(Serializer<byte[], LoginRespBean> serial, byte[] body) {
		if(null == body) return null;
		return serial.deSerialize(body, LoginRespBean.class);
	}
	
}
