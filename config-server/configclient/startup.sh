#!/bin/bash
rm -rf lib
mvn dependency:copy-dependencies -DoutputDirectory=lib -DincludeScope=runtime
nohup java -Xmx10g -Xms128m -cp target/configclient-1.0-SNAPSHOT.jar -Djava.ext.dirs=./lib com.doorcii.configserver.client.ConfigServerClientStarter >/dev/null 2>&1 &