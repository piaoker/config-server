package com.doorcii.configserver.serializers;

import java.util.HashMap;
import java.util.Map;

import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.Type;

/**
 * 获取序列化工具
 * @author Jacky
 * 2017年5月14日
 */
public class SerialSaveMap {
	public static final SerialFactory kryoFactory = new KryoSerialFactory();
	@SuppressWarnings({ "serial", "rawtypes" })
	private static Map<Byte,Serializer> serialMap = new HashMap<Byte,Serializer>(){{
		Serializer<byte[],TPacket> kryoSer = kryoFactory.createSerializer();
		this.put(Type.SERIALIZE_MASK, kryoSer);
	}};
	
	@SuppressWarnings("unchecked")
	public static <B,T> Serializer<B,T> getKryoSerializer() {
		return serialMap.get(Type.SERIALIZE_MASK);
	} 
	
	@SuppressWarnings("unchecked")
	public static <B,T> Serializer<B,T> getSerilizerByType(byte type) {
		return serialMap.get((byte)(type&Type.SERIALIZE_MASK));
	}
	
	//!TODO protobuf
	
}
