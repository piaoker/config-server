package com.doorcii.configserver.serializers;

/**
 * protobuf 序列化工具
 * @author Jacky
 * 2017年5月14日
 */
public class ProtoBufSerializer<O> implements Serializer<byte[], O> {

	@Override
	public O deSerialize(byte[] inputObject, Class<O> clazz) throws SerializeException {
		return null;
	}

	@Override
	public byte[] serialize(O inputObject) throws SerializeException {
		return null;
	}
	
}
