package com.doorcii.configserver.serializers;

/**
 * kryo反序列化工具
 * @author Jacky
 * 2017年5月14日
 */
public class KryoSerialFactory implements SerialFactory {
	
	@Override
	public <O> Serializer<byte[], O> createSerializer() {
		return new KryoSerializer<O>();
	}

}
