package com.doorcii.configserver.commons;

/**
 * 客户端信息
 * @author Jacky
 */
public class UserInfo {
	
	/**
	 * 用户名
	 */
	private String userName;
	
	/**
	 * 登陆密码
	 */
	private String password;
	
	/**
	 * 组
	 */
	private String groupName;
	
	/**
	 * 应用id
	 */
	private int appId;
	
	private String accessToken;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public static UserInfo build() {
		return new UserInfo();
	}
	
}
