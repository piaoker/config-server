package com.doorcii.configserver.handler;

import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.serializers.SerialSaveMap;
import com.doorcii.configserver.serializers.Serializer;

public abstract class ConfigAbstractHandler<T> implements ConfigBaseHandler {
	
	public abstract Object handler( T bsBody, ChannelContext<ConfigContext, TPacket, Object> channelContext) throws Exception;
	
	/**
	 * 可能我这里对泛型操作比较乱
	 * 由于kryo 在反序列化的时候一定要传一个class
	 * 所以这里比较麻烦
	 * @param serial
	 * @param body
	 * @return
	 */
	public abstract T deserial(Serializer<byte[],T> serial,byte[] body);
	
	@Override
	public Object handler(TPacket packet, ChannelContext<ConfigContext, TPacket, Object> channelContext) throws Exception {
		Serializer<byte[],T> serial =getSerializer(packet.getType());
		return handler(deserial(serial,packet.getBody()), channelContext);
	}
	
	protected <O> Serializer<byte[],O> getSerializer(byte type) {
		return SerialSaveMap.getSerilizerByType(type);
	}
}
