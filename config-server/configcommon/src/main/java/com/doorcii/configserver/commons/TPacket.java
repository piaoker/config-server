package com.doorcii.configserver.commons;

import org.tio.core.intf.Packet;

/**
 * config-server数据交互包
 * @author Jacky
 * 2017年5月14日
 */
public class TPacket extends Packet {
	/**
	 * 有人会问：为什么不直接写5？回答：如果这个B不装，那我就会死掉！
	 */
	public static final byte HEADER_LENGTH = 0B00000101;
	
	/**
	 * 消息体字节
	 */
	private byte[] body;
	/**
	 * 数据报类型
	 */
	private byte type;
	
	public TPacket (byte type) {
		this.type = type;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "TPacket [type=" + type + "]";
	}
	
	public static TPacket buildTPacket(byte type) {
		return new TPacket(type);
	}
}
