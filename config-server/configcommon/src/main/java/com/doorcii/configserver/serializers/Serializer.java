package com.doorcii.configserver.serializers;

/**
 * 序列化反序列化公共接口
 * @author Jacky
 * 2017年5月14日
 */
public interface Serializer<B,O> {
	
	O deSerialize(B inputObject,Class<O> clazz) throws SerializeException ;
	
	B serialize(O inputObject) throws SerializeException ;
}
