package com.doorcii.configserver.commons;

import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;
import org.tio.core.GroupContext;
import org.tio.core.exception.AioDecodeException;
import org.tio.core.intf.AioHandler;

/**
 * 全局抽象编码解码handler
 * @author Jacky
 * 2017年5月14日
 */
public abstract class ConfigServerAbstractHandler implements AioHandler<ConfigContext, TPacket, Object> {
	private static Logger logger = LoggerFactory.getLogger(ConfigServerAbstractHandler.class);
	
	@Override
	public ByteBuffer encode(TPacket packet, GroupContext<ConfigContext, TPacket, Object> groupContext,
			ChannelContext<ConfigContext, TPacket, Object> channelContext) {
		
		byte[] body = packet.getBody();
		long bodyLength = null == body?0L:body.length;
		if(bodyLength > Integer.MAX_VALUE) {
			throw new RuntimeException("The packet "+packet+" exceed Integer.MAX_VALUE,remote:"+channelContext.getClientNode());
		}
		
		int totalLength = TPacket.HEADER_LENGTH+(int)bodyLength; 
		ByteBuffer buffer = ByteBuffer.allocate(totalLength);
		buffer.order(groupContext.getByteOrder());
		
		buffer.put(packet.getType());
		buffer.putInt((int)bodyLength);
		if(null != body) {
			buffer.put(body);
		}
		return buffer;
	}

	@Override
	public TPacket decode(ByteBuffer buffer, ChannelContext<ConfigContext, TPacket, Object> channelContext)
			throws AioDecodeException {
		int readableLength = buffer.limit() - buffer.position();
		if (readableLength < TPacket.HEADER_LENGTH) {
			return null;
		}
		
		byte type = buffer.get();
		int bodyLength = buffer.getInt();

		if (bodyLength < 0) {
			logger.error("type={} bodyLength ={} is not right, remote:{}",type,bodyLength,channelContext.getClientNode());
			return null;
		}

		int neededLength = TPacket.HEADER_LENGTH + bodyLength;
		if ((readableLength - neededLength) < 0) {
			return null;
		} else {
			TPacket packet = TPacket.buildTPacket(type);
			if (bodyLength > 0) {
				byte[] dst = new byte[bodyLength];
				buffer.get(dst);
				packet.setBody(dst);
			}
			return packet;
		}
	}
	
	
}
