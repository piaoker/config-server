package com.doorcii.configserver.commons;

public class Constants {
	
	public static String SERVER = "192.168.0.104";
	
	public static String LINUX_SERVER = "10.1.1.110";
	
	public static int PORT = 6789;
	
	public static final byte REMOTE_SUCCESS = 0B1;
	
	public static final byte REMOTE_FAILED = 0B0;
	
	public static final int clientAppId = 1001;
	
}
