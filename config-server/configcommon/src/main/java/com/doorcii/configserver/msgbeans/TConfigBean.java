package com.doorcii.configserver.msgbeans;

import java.util.Arrays;

/**
 * 服务器列表传输bean
 * @author Jacky
 * 2017年5月14日
 */
public class TConfigBean {
	/**
	 * 推送版本号
	 */
	private long version = 0L;
	
	/**
	 * 服务组名称
	 */
	private String groupName;
	
	/**
	 * server节点列表
	 */
	private ServerNode[] serverNodes;

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public ServerNode[] getServerNodes() {
		return serverNodes;
	}

	public void setServerNodes(ServerNode[] serverNodes) {
		this.serverNodes = serverNodes;
	}

	@Override
	public String toString() {
		return "TConfigBean [version=" + version + ", groupName=" + groupName + ", serverNodes="
				+ Arrays.toString(serverNodes) + "]";
	}
	
}
