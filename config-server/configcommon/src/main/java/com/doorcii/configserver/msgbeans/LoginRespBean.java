package com.doorcii.configserver.msgbeans;

import com.doorcii.configserver.commons.Constants;

/**
 * 鐧婚檰鍝嶅簲bean
 * @author Jacky
 * 2017骞�5鏈�14鏃�
 */
public class LoginRespBean {
	
	private byte code;
	
	private String accessToken;
	
	private String errorMsg;
	
	private long loginTime;

	public byte getCode() {
		return code;
	}

	public void setCode(byte code) {
		this.code = code;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	public long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}
	
	public boolean isLoginSuc() {
		return code == Constants.REMOTE_SUCCESS;
	}

	@Override
	public String toString() {
		return "LoginRespBean [code=" + code + ", accessToken=" + accessToken + ", errorMsg=" + errorMsg + "]";
	}
	
	
}
