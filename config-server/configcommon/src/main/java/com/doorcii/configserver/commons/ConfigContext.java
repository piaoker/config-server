package com.doorcii.configserver.commons;

/**
 * config-server请求上下文
 * @author Jacky
 * 2017年5月14日
 */
public class ConfigContext {
	
	private UserInfo userInfo;
	
	public ConfigContext() {
	}

	public ConfigContext(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
}
