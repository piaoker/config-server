package com.doorcii.configserver.serializers;

public interface SerialFactory {
	
	public <O> Serializer<byte[],O> createSerializer();
	
}
