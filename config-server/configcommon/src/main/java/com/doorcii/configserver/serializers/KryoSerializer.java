package com.doorcii.configserver.serializers;

import java.io.ByteArrayOutputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * Kryo 
 * @author Jacky
 * 2017年5月14日
 */
public class KryoSerializer<T> implements Serializer<byte[],T>{
	
	@Override
	public T deSerialize(byte[] inputObject,Class<T> clazz) throws SerializeException {
		if(null == inputObject) return null;
		Kryo kryo = new Kryo();
		Input input = new Input(inputObject);
		T t = null;
		try {
			t = (T)kryo.readObject(input,clazz);
		} catch (Exception e) {
			throw new SerializeException(e);
		} finally {
			input.close();
		}
		return t;
	}

	@Override
	public byte[] serialize(T inputObject) throws SerializeException {
		Kryo kryo = new Kryo();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Output output = new Output(os);
		try {
			kryo.writeObject(output, inputObject);
		} catch(Exception e) {
			throw new SerializeException(e);
		} finally {
			output.close();
		}
		return os.toByteArray();
	}
	
	public static void main(String[] args) {
		KryoSerializer<String> ser = new KryoSerializer<>();
		byte [] b = ser.serialize("1234草榴");
		System.out.println(ser.deSerialize(b,String.class));
	}

}
