package com.doorcii.configserver.msgbeans;

/**
 * 已授权基类
 * @author Jacky
 */
abstract class AuthedBaseBean {
	
	protected String accessToken;

	public String getAccessToken() {
		return accessToken;
	}

	public abstract void setAccessToken(String accessToken) ;
	
}
