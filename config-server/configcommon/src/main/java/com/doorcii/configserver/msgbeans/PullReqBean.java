package com.doorcii.configserver.msgbeans;

/**
 * 拉取配置请求
 * @author Jacky
 */
public class PullReqBean extends AuthedBaseBean {
	
	/**
	 * 是否需要拉取子组机器列表
	 */
	private boolean needSub;
	
	public PullReqBean(){}
	
	public PullReqBean(String accessToken) {
		setAccessToken(accessToken);
	}

	public boolean isNeedSub() {
		return needSub;
	}

	public void setNeedSub(boolean needSub) {
		this.needSub = needSub;
	}

	@Override
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
