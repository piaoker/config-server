package com.doorcii.configserver.msgbeans;

/**
 * 登陆请求bean
 * @author Jacky
 */
public class LoginReqBean {
	private String userName;
	private String password;
	private String groupName;
	private int appId;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	@Override
	public String toString() {
		return "LoginReqBean [userName=" + userName + ", password=" + password + ", groupName=" + groupName + ", appId="
				+ appId + "]";
	}
}
