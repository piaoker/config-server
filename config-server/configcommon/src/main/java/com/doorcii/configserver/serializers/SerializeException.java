package com.doorcii.configserver.serializers;

/**
 * 序列化异常
 * @author Jacky
 * 2017年5月14日
 */
public class SerializeException extends RuntimeException {
	private static final long serialVersionUID = -1168601847107766826L;
	public SerializeException(Exception e) {
		super(e);
	}
}
