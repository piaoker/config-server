package com.doorcii.configserver.handler;

import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;

public interface ConfigBaseHandler {
	
	public Object handler(TPacket packet, ChannelContext<ConfigContext, TPacket, Object> channelContext)  throws Exception;
	
}
