package com.doorcii.configserver.msgbeans;

import org.apache.commons.lang3.StringUtils;

/**
 * 服务器节点信息
 * @author Jacky
 * 2017年5月14日
 */
public class ServerNode {
	
	static String defaultIP = "127.0.0.1";
	
	static int defaultPort = 8080;
	
	/**
	 * 服务器IP地址
	 */
	private String ip;
	/**
	 * 服务器端口
	 */
	private int port;
	
	/**
	 * 权重
	 */
	private byte weight;
	
	public ServerNode () {
	}
	
	public ServerNode (String ip,Integer port) {
		this.ip = (StringUtils.isBlank(ip)?defaultIP:ip);
		this.port = (null == port?defaultPort:port);
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	public byte getWeight() {
		return weight;
	}

	public void setWeight(byte weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "ServerNode [ip=" + ip + ", port=" + port + ", weight=" + weight + "]";
	}
	
}
