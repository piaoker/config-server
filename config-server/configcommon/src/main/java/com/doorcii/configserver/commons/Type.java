package com.doorcii.configserver.commons;

/**
 * 数据报文类型
 * @author Jacky
 * 2017年5月14日
 * 全部用二进制表示出来可能会更容易理解一点
 * 使用 0xBA这种不太好理解位
 * 0B01000000 第二位1用来标示使用kryo序列化 0则使用protobuf
 */
public class Type {
	/**
	 * 序列化方式掩码
	 */
	public static final byte SERIALIZE_MASK = 0B01000000;
	/**
	 * 心跳类型
	 */
	public static final byte HEART_BEAT_PACKET = 0B01000000;
	/**
	 * 登陆请求包
	 */
	public static final byte LOGIN_REQ_PACKET = 0B01000001;
	
	/**
	 * 登陆响应包
	 */
	public static final byte LOGIN_RESP_PACKET = 0B01000010;
	
	/**
	 * 加入订阅组
	 */
	public static final byte JOIN_GROUP_REQ_PACKET = 0B01000011;
	
	/**
	 * 加入订阅组响应
	 */
	public static final byte JOIN_GROUP_RESP_PACKET = 0b01000100;
	
	
	
	/**
	 * IP列表类型,第二位为kryo序列化标示
	 */
	public static final byte IP_LIST_REQ_PACKET = 0B01001110;
	
	/**
	 * IP列表类型响应
	 */
	public static final byte IP_LIST_RESP_PACKET = 0B01001111;
	
	/**
	 * 0 protobuf
	 * 64 kryo
	 * 获取序列化方式
	 * @param type
	 * @return
	 */
	public static final byte getSerializeType(byte type) {
		return (byte)(SERIALIZE_MASK & type);
	}
}
