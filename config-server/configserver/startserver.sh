#!/bin/bash
rm -rf lib
mvn dependency:copy-dependencies -DoutputDirectory=lib -DincludeScope=runtime
nohup java -server -Xloggc:/root/tio/logs/jvm.log -server -Xmx7g -Xms128m -cp target/configserver-1.0-SNAPSHOT.jar -Djava.ext.dirs=./lib com.doorcii.configserver.server.ConfigServerStarter >/dev/null 2>&1 &