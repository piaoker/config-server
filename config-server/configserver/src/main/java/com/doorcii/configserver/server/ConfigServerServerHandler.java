package com.doorcii.configserver.server;

import java.util.HashMap;
import java.util.Map;

import org.tio.core.ChannelContext;
import org.tio.server.intf.ServerAioHandler;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.ConfigServerAbstractHandler;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.Type;
import com.doorcii.configserver.handler.ConfigAbstractHandler;
import com.doorcii.configserver.handlers.HeartbeatReqHandler;
import com.doorcii.configserver.handlers.LoginReqHandler;
import com.doorcii.configserver.handlers.PullServiceHandler;

public class ConfigServerServerHandler extends ConfigServerAbstractHandler implements ServerAioHandler<ConfigContext, TPacket, Object> {
	private static Map<Byte,  ConfigAbstractHandler<?>> handlerMap = new HashMap<>();
	static {
		handlerMap.put(Type.LOGIN_REQ_PACKET, new LoginReqHandler());
		handlerMap.put(Type.HEART_BEAT_PACKET, new HeartbeatReqHandler());
		handlerMap.put(Type.IP_LIST_REQ_PACKET, new PullServiceHandler());
	}
	@Override
	public Object handler(TPacket packet, ChannelContext<ConfigContext, TPacket, Object> channelContext)
			throws Exception {
		ConfigAbstractHandler<?> handler = handlerMap.get(packet.getType());
		if(null != handler) handler.handler(packet, channelContext);
		return null;
	}
	
}
