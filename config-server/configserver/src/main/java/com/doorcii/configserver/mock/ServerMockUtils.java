package com.doorcii.configserver.mock;

import com.doorcii.configserver.msgbeans.ServerNode;
import com.doorcii.configserver.msgbeans.TConfigBean;

public class ServerMockUtils {
	
	public static TConfigBean getMockServer(String groupName) {
		TConfigBean serverBean = new TConfigBean();
		ServerNode[] servers = new ServerNode[] {new ServerNode("192.168.70.250",80),new ServerNode("10.1.1.102",22),new ServerNode("10.1.1.110",22),new ServerNode("10.1.1.222",22),};
		serverBean.setServerNodes(servers);
		serverBean.setGroupName(groupName);
		serverBean.setVersion(1L);
		return serverBean;
	}
}
