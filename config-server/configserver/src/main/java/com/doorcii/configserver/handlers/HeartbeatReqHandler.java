package com.doorcii.configserver.handlers;

import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.handler.ConfigAbstractHandler;
import com.doorcii.configserver.msgbeans.HeartBeatBean;
import com.doorcii.configserver.serializers.Serializer;

/**
 * 心跳处理
 * 毫无处理逻辑
 * @author Jacky
 * 2017年3月27日 下午9:51:28
 */
public class HeartbeatReqHandler extends ConfigAbstractHandler<HeartBeatBean> {
	public HeartbeatReqHandler() {
	}

	
	@Override
	public Object handler(HeartBeatBean bsBody, ChannelContext<ConfigContext, TPacket, Object> channelContext)
			throws Exception {
		return null;
	}


	@Override
	public HeartBeatBean deserial(Serializer<byte[], HeartBeatBean> serial, byte[] body) {
		return null;
	}


	@Override
	public Object handler(TPacket packet, ChannelContext<ConfigContext, TPacket, Object> channelContext)
			throws Exception {
		return null;
	}

}
