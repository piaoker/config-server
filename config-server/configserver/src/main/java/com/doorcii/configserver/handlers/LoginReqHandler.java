package com.doorcii.configserver.handlers;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.tio.core.Aio;
import org.tio.core.ChannelContext;
import org.tio.core.utils.SystemTimer;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.Constants;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.commons.Type;
import com.doorcii.configserver.commons.UserInfo;
import com.doorcii.configserver.handler.ConfigAbstractHandler;
import com.doorcii.configserver.msgbeans.LoginReqBean;
import com.doorcii.configserver.msgbeans.LoginRespBean;
import com.doorcii.configserver.serializers.Serializer;

public class LoginReqHandler extends ConfigAbstractHandler<LoginReqBean> {

	@Override
	public Object handler(LoginReqBean bsBody,
			ChannelContext<ConfigContext, TPacket, Object> channelContext) throws Exception {
		if(null == bsBody) return null;
		String name = bsBody.getUserName();
		String password = bsBody.getPassword();
		int appId = bsBody.getAppId();
		
		LoginRespBean resp = new LoginRespBean();
		TPacket tpacket = TPacket.buildTPacket(Type.LOGIN_RESP_PACKET);
		// 登陆简单校验,如果有group，还应当校验group是否允许
		if(StringUtils.isBlank(name) || StringUtils.isBlank(password) || appId < 1) {
			resp.setCode(Constants.REMOTE_FAILED);
			resp.setErrorMsg("privilege invalid");
		} else {
			resp.setAccessToken(UUID.randomUUID().toString());
			resp.setCode(Constants.REMOTE_SUCCESS);
			resp.setLoginTime(SystemTimer.currentTimeMillis());
			
			UserInfo user = UserInfo.build();
			user.setAccessToken(resp.getAccessToken());
			user.setAppId(appId);
			user.setGroupName(bsBody.getGroupName());
			/**
			 * 绑定关系
			 */
			successBind(channelContext,user);
		}
		Serializer<byte[], LoginRespBean> serializer = getSerializer(Type.LOGIN_REQ_PACKET);
		byte[] body = serializer.serialize(resp);
		tpacket.setBody(body);
		Aio.send(channelContext, tpacket);
		return null;
	}
	
	/**
	 * 登陆成功则放入session
	 * @param userContext
	 * @param bsBody
	 * @param resp
	 */
	private void successBind(ChannelContext<ConfigContext, TPacket, Object> channelContext,UserInfo userInfo) {
		ConfigContext sessionContext = new ConfigContext();
		sessionContext.setUserInfo(userInfo);
		channelContext.setSessionContext(sessionContext);
		
		/**
		 * 绑定用户和组关系
		 */
		Aio.bindUser(channelContext, userInfo.getAccessToken());
		String group = userInfo.getGroupName();
		if(StringUtils.isNotBlank(group)) {
			Aio.bindGroup(channelContext, group);
		}
	}

	@Override
	public LoginReqBean deserial(Serializer<byte[], LoginReqBean> serial, byte[] body) {
		if(null == body) return null;
		return serial.deSerialize(body, LoginReqBean.class);
	}
	
}
