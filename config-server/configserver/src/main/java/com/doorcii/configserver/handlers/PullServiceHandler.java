package com.doorcii.configserver.handlers;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.TPacket;
import com.doorcii.configserver.handler.ConfigAbstractHandler;
import com.doorcii.configserver.mock.ServerMockUtils;
import com.doorcii.configserver.msgbeans.PullReqBean;
import com.doorcii.configserver.msgbeans.TConfigBean;
import com.doorcii.configserver.serializers.Serializer;
import com.doorcii.configserver.services.ConfigService;

public class PullServiceHandler extends ConfigAbstractHandler<PullReqBean> {
	private static Logger logger = LoggerFactory.getLogger(PullServiceHandler.class);
	@Override
	public Object handler(PullReqBean bsBody, ChannelContext<ConfigContext, TPacket, Object> channelContext)
			throws Exception {
		if(null == bsBody) return null;
		if(StringUtils.isBlank(bsBody.getAccessToken())) {
			logger.error("Request access is null.userName:{}",channelContext.getSessionContext().getUserInfo().getUserName());
			return null;
		}
		TConfigBean configBean = ServerMockUtils.getMockServer(channelContext.getSessionContext().getUserInfo().getGroupName());
		ConfigService.sendConfigInfo(configBean, channelContext);
		
		return null;
	}

	@Override
	public PullReqBean deserial(Serializer<byte[], PullReqBean> serial, byte[] body) {
		if(null == body) return null;
		return serial.deSerialize(body, PullReqBean.class);
	}
	
}
