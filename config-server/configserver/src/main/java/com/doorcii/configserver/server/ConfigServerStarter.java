package com.doorcii.configserver.server;

import java.io.IOException;

import org.tio.core.DefaultAioListener;
import org.tio.server.AioServer;
import org.tio.server.ServerGroupContext;
import org.tio.server.intf.ServerAioHandler;
import org.tio.server.intf.ServerAioListener;

import com.doorcii.configserver.commons.ConfigContext;
import com.doorcii.configserver.commons.Constants;
import com.doorcii.configserver.commons.TPacket;

/**
 * config-server 服务端启动
 * @author Jacky
 * 2017年5月14日
 */
public class ConfigServerStarter {
	
	static ServerAioHandler<ConfigContext, TPacket, Object> aioHandler = new ConfigServerServerHandler();
	static ServerAioListener<ConfigContext, TPacket, Object> aioListener = new DefaultAioListener<ConfigContext, TPacket, Object>();
	static ServerGroupContext<ConfigContext, TPacket, Object> serverGroupContext = new ServerGroupContext<>(aioHandler, aioListener);
	static AioServer<ConfigContext, TPacket, Object> aioServer = new AioServer<>(serverGroupContext); //可以为空
	
	static String serverIp = null;
	static int serverPort = Constants.PORT;

	public static void main(String[] args) throws IOException {
		aioServer.start(serverIp, serverPort);
	}
	
}
